#!/bin/bash
# A quick shell script to install Arch Linux in a VM
# which is only for the purpose of getting arch quick, rather than going through menus and selecting options.

timedatectl set-ntp true
fdisk -n /dev/sda
mkfs.ext4 /dev/sda1
mount /dev/sda1 /mnt
pacstrap /mnt base linux linux-firmware nano vim neofetch
genfstab -U /mnt >> /mnt/etc/fstab

